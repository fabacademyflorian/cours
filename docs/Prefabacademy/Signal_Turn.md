# Project : Voice-controlled Turn Lights

The goal of this project is to reproduce the Voice-Turn project without an Arduino card and adding LED controls. I will be incorporeting an ATMEga 328P U-PH directly into a copper card. It's usefull to win some space on the whole device when you know exactly what you want to do.

<img src="docs/Images3/2021-10-05_17_33_07-VoiceTurn.png" style="display: block; margin: 0 auto;" height="" width="">

This project is under the MIT license.

## Incorporeting Microcontroller onto the copperboard

The devices I will need on the board :

Exterior communication :

  - Connection to battery (+ and -)

  - 4 pins for left LEDS and 4 pins for right LEDS

  - 8 LEDS

  - Cable jack or sheath with 10(?) wires of 2 meters to connect the board to LEDS

  - 2 buttons in case voice recognition doesn't work

Microcontroller

  - ATMega 328P U-PH

  - Resistance 10K between reset pin and Vcc

  - button to reset

  - add a LED to get a signal (one is enough but 2 LEDs is better to know if left or right has been chosen)

  - A capacitor between GND and Vcc ->

  - (add a Crystal of 16 MHz between XTAL1 and XTAL2 pins - optional)

OP Amp with microphone

  - Capacitor microphone

  - OP amplifier

  - 3 resistances of 100k and one of 2.2K

  - one potentiometer of 10K

  - one capacitor of 0.1uF

To get started, I checked the anti-viral dispositif of Jonah which used the same microcontroller to get an idea of the disposition of the elements :

|Schematic|Board|Copperboard|
|-|-|-|
|<img src="docs/Images3/2021-10-04_14_08_26-1_Schematic.png" width="">|<img src="docs/Images3/2021-10-04_14_05_33-2_Board.png" width="">|<img src="docs/Images3/Anti_viral_board.JPG" width="">|

Next schema is mendatory to know what does each pin of the microcontroller and also what are their number.

|ATMega 328P pinout diagram|Microcontroller schematic on eagle|
|-|-|
|<img src="docs/Images3/2021-10-05_17h32_52.png" width="">|<img src="docs/Images3/comingsoonTM.png" width="">|

## Incorporeting the OP Amplifier

To get a strong enough signal from the microphone we need an OP Amplifier.
The next schema shows an amplifier which can multiply at least by ten the signal. I got it from this Instructable page which is the documentation I relied on to build the OP Amp: https://www.instructables.com/Arduino-Audio-Input/

|OP Amp schematic|OP Amp pins|OP Amp pins description|
|-|-|-|
|<img src="docs/Images3/2021-10-07_18_04_16-Arduino_Audio_Input.png" width="">|<img src="docs/Images3/2021-10-07_18_03_55-50.png" width="">|<img src="docs/Images3/2021-10-07_18_06_21-50.png" width="">|

First, I wanted to test the amplification without the microphone. I connected a signal generator (here set to 53HZ, 300mV) and send a sinusoid. We can see the result in the next pictures. The input signal is in green and the output signal is in yellow. And the amplification works!

|Setup|Result with potentiometer set to max|Potentiometer set to min|
|-|-|-|
|<img src="docs/Images3/Setup.JPG" width="">|<img src="https://i.imgur.com/ciQ3Teu.png" width="">|<img src="https://i.imgur.com/WCMURTz.png" width="">|

/*Then, I searched how to connect a capacitiv microphone :

| Schema of capacitiv microphone|Description of the role of capacitor and resistor|
|-|-|
|<img src="docs/Images3/2021-10-08_16_22_55-Electronique.png" width="">|<img src="docs/Images3/2021-10-08_16_24_21-Electronique.png" width="">|

Then, I tested the microphone alone. The signal is about 500mV on the oscilloscope :

|Test of the microphone without amplification|Signal on oscilloscope|
|-|-|
|<img src="docs/Images3/Micro_seul.JPG" width="">|<img src="https://i.imgur.com/7xNvM8S.png" width="">|

Next step with microphone and amplification. After many attempts, corrections and tweaking, I finally got amplified signals on the oscilloscope. Input signal is in green (about 200mV here) and output signal in yellow (about 15  times more):

|First iteration of protobard|Last iteration|Signal with and without amplification|
|-|-|-|
|<img src="docs/Images3/OPAMP_proto1.JPG" width="">|<img src="docs/Images3/OPAMP_proto2.JPG" width="">|<img src="https://i.imgur.com/J2K7kmW.png" width="">|
*/

Last task of the week was to test the signal received on an arduino board. The Result was not the expected one since the signal is at 1023, the max of the analogic input. this may be due to an offset :

<img src="docs/Images3/2021-10-08_18_30_16-COM3.png" width="300">

The code I used :

```
int incomingAudio;

void setup() {
  Serial.begin(9600);
  pinMode(A0, INPUT);
}

void loop() {
  incomingAudio=analogRead(A0);
  Serial.print(0);                   // the print(0) and print(1023) lines are for boundaries to avoid that the plotter auto calibrate
  Serial.print("\t");
  Serial.print(1023);
  Serial.print("\t");
  Serial.println(incomingAudio);
}
```

So this conclude my work of the week. I was able to get an amplified signal. Next week, I will try to get a signal on arduino and finish the schematic and board on the software Eagle.

# Week 4

## getting a signal on Arduino :

First thing, I reproduced the setup I've done last week and tested it again with the oscilloscope. The goal is to be sure that, before testing the input in the arduino, the setup is working properly. Since the signal was not good on the last try, I need to be sure that every other step works properly.

First test of the day with the microphone :

<img src="docs/Images4/2021-10-11_15_03_31-COM3.png" width="">

Once again, the signal is not the expected one. The arduino saturate.
I was not convinced by the results of the microphone. It gave signal on the oscilloscope when I was not solicitating it and didn't gave signal when I put a speaker right above it.
For all those reasons I decided to work with the generator of signals again. If it works well, I'll know that the problem comes from the mic.

Protoboard :
<img src="docs/Images4/protoboard+arduino.JPG" width="300">

|Setup with a sort of impulsion signal|Signal on the oscilloscope|Signal on the arduino|
|-|-|-|
|<img src="docs/Images4/setup_impulsion.JPG" width="">|<img src="https://i.imgur.com/hQFQuIX.png" width="">|<img src="docs/Images4/2021-10-11_17_46_21-50Hz_impulsion.png" width="">|

Same thing with a sinusoidal signal :

|Setup with a sinusoidal signal|Signal on the oscilloscope|Signal on the arduino|
|-|-|-|
|<img src="docs/Images4/protoboard+arduino.JPG" width="">|<img src="https://i.imgur.com/gmb24Hl.png" width="">|<img src="docs/Images4/2021-10-11_15_28_52-2KHz.png" width="">|

We have a signal on the computer ! The code works and the setup too. All I need now is to put the mic on a good setup. Maybe it's the branching, maybe it's the value of the resistor and the capacitor which are not good. Maybe the electret mic is not well suited for this type of experience.

But before I test the mic, I'll make some test to see the limits of the arduino.

|Signal of 2KHz|Signal of 2KHz|Signal of 8KHz|
|-|-|-|
|<img src="https://i.imgur.com/ngsACIU.png" width="">|<img src="docs/Images4/2021-10-11_15_28_52-2KHz.png" width="">|<img src="docs/Images4/2021-10-11_15_30_52-8KHz.png" width="">|

The arduino doesn't seem to behave the same with different frequencies. It gives a proper signal when signal is 2KHz and 8KHz. But not so much on other frequencies (the setup is 200mV, an offset of 100mV and R2 = 10Kohm):

|Signal of 200Hz|Signal of 500Hz|Signal of 3.5KHz|
|-|-|-|
|<img src="docs/Images4/2021-10-11_17_52_45-200Hz.png" width="">|<img src="docs/Images4/2021-10-11_17_55_13-500Hz.png" width="">|<img src="docs/Images4/2021-10-11_18_00_44-3.5KHz.png" width="">|

For a reminder of the non-inverting amplifier, the ratio of amplification is A = 1 + R1/R2 in the following setup :

<img src="docs/Images4/Non-Inverting-Amplifier.png" width="300">

With that in mind, I made further tests to see the limits of input of the arduino :  

|Signal of 120mV and R2=5Kohm|Signal of 200mV and R2=10Kohm|Saturation of arduino when 210mV and R2=10Kohm|
|-|-|-|
|<img src="docs/Images4/2021-10-11_15_43_42-5Kohm_120mV.png" width="">|<img src="docs/Images4/2021-10-11_15_46_13-10Kohm_200mV.png" width="">|<img src="docs/Images4/2021-10-11_15_50_55-10Kohm_210mV.png" width="">|

In the first picture the ratio of amplification is A = 1 + 5/100 = 21. In the 2 others pictures the amplification is A = 1 + 10/100 = 11. When we have an entry signal of 210mV and R2=20Kohm, the output is 11*420 = 4.62V. Which is close to the limit of 5V of the Arduino. This is why we see that there is saturation.

## Getting a signal with the electret microphone

I made some further research to see how to get a signal with the electret mic. I stambled upon this response on a forum :

<img src="docs/Images4/2021-10-12_12_13_47-forum.png" width="300">

To summ up :
signal output of an electret : ~10mV

Power supply : the best is 9V with a resistance of 10Kohm

So the oscilloscope needs to be set up to ~10mV. Also if I want both the electret and the Amp to work with the same power supply I'll need to add a resistance before the Amp because the max voltage it can receive is 5.5V.
Next picture is the note I took after my research :

<img src="docs/Images4/Notes.JPG" width="300">

I add a resistance of 80Kohm on the power supply of the amp. And for the amplification, R1=1Mohm and R2=4Kohm so we have A = 1 + 1000/4 = 251. Enough to go from 10mv to 2.5V. At least, it is what I expected :

|Schematic|Protoboard|signal - range of 20mV|
|-|-|-|
|<img src="docs/Images4/2021-10-12_15_42_28-1_Schematic.png" width="">|<img src="docs/Images4/protoboard_mic+amp6241.JPG" width="">|<img src="https://i.imgur.com/A4ZHIih.png" width="">|

So we have no signal when clapping or whistling but a lot of noise. This setup doesn't work.

Once again, in order to identify the problem, I tried to narrow the components. I know the setup of the amp works (at least for an amplification of 10 to 20), so I'll just check the mic alone :

|Schematic|Protoboard|signal - range of 10mV|
|-|-|-|
|<img src="docs/Images4/2021-10-12_16_17_56-1_Schematic.png" width="">|<img src="docs/Images4/protoboard_micro_capa.JPG" width="">|<img src="https://i.imgur.com/tvyHRoY.png" width="">|

Not much more result here even with the oscilloscope set to 10mV.

Maybe it's coming from the entry of the oscilloscope. So I switched from entry 2 to entry 1 :

<img src="https://i.imgur.com/eo8kuJV.png" width="300">

Maybe it's coming from the resistance or the power supply. So I made a test protocol with different value of resistor and Voltage. All the results gave the same signals :

<img src="docs/Images4/protocol_de_test.JPG" width="300">

I didn't have any convincing results either, so I remove the capacitor as a last resort. And it worked ! I finally have some result with the electret :

|Schematic|Protoboard|Fourier signal|
|-|-|-|
|<img src="docs/Images4/2021-10-12_16_25_39-1_Schematic.png" width="">|<img src="docs/Images4/protoboard_microseul.JPG" width="">|<img src="https://i.imgur.com/m3gfCIS.png" width="">|

The math fonction of Fourier signal on the oscilloscope allows to see an amplitude as a fonction of frequence. Here we can see a peak at 2.18KHz which is the frequence which I whistled.

Here is a video of the signal according to the whistle :

<iframe
    width="640"
    height="480"
    src="https://youtu.be/LNKX3BX_0UI"
    frameborder="0"
    allow="autoplay; encrypted-media"
    allowfullscreen
</iframe>

## Incorporeting the microphone + the Amplifier


Now that the microphone and the amplifier work separately, let's make them work together. I send the note A 440Hz coming from a diapason (recorded on Youtube).

This is the setup I used :

|Schematic|Protoboard|
|-|-|
|<img src="docs/Images4/2021-10-13_15_02_15-1_Schematic_mic_amp.png" width="350">|<img src="docs/Images4/protoboard_mic_amp6241.JPG" width="350">|

The result on the oscilloscope :

|Input signal of &Delta;(y2)=7.5mV|Ouptut signal of &Delta;(y1)=62.5mV|
|-|-|
|<img src="https://i.imgur.com/FVQrNFu.png" width="">|<img src="https://i.imgur.com/iqFRh8r.png" width="">|

Now I have an ouptut of roughly 8 times the input. The goal is to have an amplification of at least 100 to 200 for an optimal input in the arduino.

I will make a test protocol with the 3 different amp there is in the fablab and different amplification. my goal is to see if there is any signal amplified in the output.

|Amplification/OP Amp|MCP6241|LM386|||LM741|||
|-|-|-|-|-|-|-|-|
|Supply voltage|5V|5V|10V|15V|10V|15V|18V|
|19 (R1=100K, R2=5,6K)|A=5.9|no output, too much bruit|no output|no output|no output|no output|no output|
|85 (R1=470K, R2=5,6K)|no output|no output, too much bruit|no output|no output|no output|no output|no output|

The more power there is on the supply voltage (in the limit of what can afford the op amp), the cleaner the signal is. The setup I made worked for the MCP6241 until a certain amplification (about 20) but it didn't work for the LM386 and LM741.

Here is the signal the most amplified I obtained :

|Input signal &Delta;(y1)=26mV|Output signal &Delta;(y2)=455mV|
|-|-|
|<img src="docs/Images4/Amp_y1.png" width="">|<img src="docs/Images4/Amp_y2.png" width="">|


----------------------------------------------------------

I need to find a way to operate the LM386 and LM741. I searched for more info on the web and found this schematic which worked to amplify a signal on the Arduino:

<img src="docs/Images4/2021-10-15_11_48_46-Arduino_LM386.png" align="vertical-align: -moz-middle-with-baseline;" width="400">

Unfortunatly I had no more success with this setup. There is too much noise when I plug the amp :

<img src="docs/Images4/Noise.png" width="400">

I will stop for now my investigations on op amps because I've already put too much time in it. I was able to obtain a x18 amplification, if I really want to get a x300 amp, I'll just have to put 2 serial MCP6241 OP amp.

-------------------------------------------------------
# Change of plan (week 5)

After a closer look to the spec needed for the microcontroller for this project, the Arduino Uno (with the ATMega 328P) is not powerful enough to work with machine learning. According to the Edge Impulse Website, we need 64Kbits of SRAM minimum and an ARM microcontroller.

<img src="docs/Images4/2021-10-18_16_20_44-Edge_Impulse.png" width="">

Since there is no Arduino nano 33 BLE sense in the Fablab (or no equivalent), I will change plan for the project. I'm going to do the switch from a voice controlled turn lights to a switch controlled turn light.

## Prototyping switch + led system

I made a small schematic with 4 LEDs and 2 switch which will be enough to test the code :

|Schematic|Proto + Arduino|
|-|-|
|<img src="docs/Images4/2021-10-18_18_48_00-1_Schematic_sw_led.png">|<img src="docs/Images5/proto_arduino.JPG">|

Here the code I used for the Arduino. The goal is to make the LEDs blink to one way or the other depending on the switch you click on :

```
#define LED1 3
#define LED2 4
#define LED3 5
#define LED4 6
#define button1 8
#define button2 9
#define ACTIVATED LOW

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(button1, INPUT);
  digitalWrite(button1, HIGH);
  pinMode(button2, INPUT);
  digitalWrite(button2, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  while(digitalRead(button1) == ACTIVATED){       // when button1 is pushed, LEDs blink to the right
    digitalWrite(LED1, HIGH);
    delay(500);
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, HIGH);
    delay(500);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, HIGH);
    delay(500);
    digitalWrite(LED3, LOW);
    digitalWrite(LED4, HIGH);
    delay(500);
    digitalWrite(LED4, LOW);
  }

  while(digitalRead(button2) == ACTIVATED){       // when button2 is pushed, LEDs blink to the left
    digitalWrite(LED4, HIGH);
    delay(500);
    digitalWrite(LED4, LOW);
    digitalWrite(LED3, HIGH);
    delay(500);
    digitalWrite(LED3, LOW);
    digitalWrite(LED2, HIGH);
    delay(500);
    digitalWrite(LED2, LOW);
    digitalWrite(LED1, HIGH);
    delay(500);
    digitalWrite(LED1, LOW);
  }
}
```

## Adding the ATMega 328

Choice of battery : The ATMega 328 needs a range of voltage to work (2.7V to 5.5V for the ATMega 328P). To make it work with a higher power supply, we need to add a regulator which will bring the tension from X Volts to 5 Volts. Also there is a choice of design between choosing a more powerfull battery therefore more longlasting or a smaller battery without any regulator which won't waste any energy.

Possibilities
- Adding a battery of 5V without regulator
- Battery of 3.7V
- lithium Battery of 3V
- Battery of 9V with a regulator

Choice of LEDs : LED of 5mm of 376 mcd for typical luminosity instead of LED SMD of 25 mcd for better luminosity (there will also be some kind of mirror ring to diffract more light)

Calcul of circuit consumption :
According to the LED data sheet : 90 mW of power disspation
 -> for 24 LEDs in 10 sec : 24 * 90 / 360 = 6 mWh
Let's say 20 turns in 30 min -> 20 * 6 = 120 mWh

the consumption of the ATMega 328P is 1.5 mA at 3V - 4MHz. It may not be the power supply and frequency I'll use, but should be well enough for an estimation of power consumption -> P = 4.5 mW -> 2.25 mWh in 30 min

So we have a consumption of 122.5 mWh in a 30 min ride. This is 40 mAh of battery consumption.
For a lithium battery of 3V and 225 mAh, it's about 5 or 6 rides.
For a USB battery of 5V and 5000 mAh, it's about 125 rides. Which is much better and it's rechargeable. I will opt for the Batterie externe USB UPBK5002BK from GoTronic.

### Programming the ATMega 328

To start the programming, I drawed the circuit on Eagle :

<img src="docs/Images7/2021-11-04_16_24_33-1.png" width="550">

To do it on the breadboard next. For the power supply I used a 5V supply, so no need for a regulator. I made a few iteration of breadboard, the first one integrated all the components of the circuits. The last one was just the barebone circuit to program the ATMega to prevent any external source of error :

|1st circuit|Last iteration|
|-|-|
|<img src="docs/Images7/Breadboard1.JPG" width="">|<img src="docs/Images7/Breadboard2.JPG" width="">|

To connect the pins I checked the pinout diagram of the ATMega 328.

<img src="docs/Images7/2021-11-04_16_49_03.png" width="">

And the pinout diagram for the isp connector of the Atmel license

<img src="docs/Images7/2021-11-04_16_49_14.png" width="">

For the software, I used Atmel studio. I first wanted to burn the bootloader to be able to put the code directly from the arduino IDE to the microcontroller. After a few a try and some tweaks to my circuit, I decided to change and apply the solution recommended by Jonah which is to use the ATmel studio software each time I want to put a code in the chip. Here are the steps once the Atmel ICE is plugged :

- File new project :

<img src="docs/Images7/2021-11-04_16_57_52.png" width="">

- Choose GCC C executable Project

- Choose Tool : ATmel ICE, Device ATMega 328, Interface ISP

<img src="docs/Images7/2021-11-04_17_00_03.png" width="">

Click read next to device signature, to make sure that you can communicate with the ATMega 328. In the fuse tab, you can unselect the CKDiv8 fuse (this fuse divide by 8 the frequence of the microcontroller). You can see the use of eache fuse in the enengbedded website : https://www.engbedded.com/fusecalc/

<img src="docs/Images7/2021-11-04_17_04_16.png" width="">

In Memories tab, you can add the code from Arduino. Here I used the one from the exemples of blink Led.

The code must be a .hex file. You can see in the compiler where the .hex file has been saved. But before that you may need to check if the option to display the results during compilation is selected in File -> Preferences :

|Select compilation/téléversement|Copy the path to the .hex file|
|-|-|
|<img src="docs/Images7/preferences_arduino.png" width="">preferences_arduino|<img src="docs/Images7/2021-11-05_12_46_46.png" width="">|

Click on the file line and add the .hex file :

<img src="docs/Images7/2021-11-04_17_11_10.png" width="">

Then click on program.
