# Electronics

First lessons I had in ages on electronics. Despite having learn all this before, it's good to have a little reminder of it.
I liked the comparison Jonah made between tension and the need of parisians to go to the beach -> the more parisians who want to go to the beach and the less people there are on the beach, the more tension there is.
Roads and means to go there work as current where the flow of parisians will be impeded by the curve of the road and the number of roadlines(the decrease of roadlines will act as a resistance).

Ohm's law : U = RI

## First circuit, made on protoboard

I made the first circuit with Marc, the trainee at the fablab. The goal was to read the schema below, find the different componants and assemble them on the protoboard.

<img src="docs/Images/1er_circuit.JPG" alt="schéma branchements" classe="rotate90">

We tested some devices with multimeter to check the value of the resistances

![image](docs/Images/1er_circuit_protoboard2.JPG "cicuit sonore")

It was a fun little circuit which allowed for doing some sound which could be changed thanks to the potentiometers.

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1139260834&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/user-887841969" title="Flo" target="_blank" style="color: #cccccc; text-decoration: none;">Flo</a> · <a href="https://soundcloud.com/user-887841969/circuits-sons-etranges" title="Circuits Sons Etranges" target="_blank" style="color: #cccccc; text-decoration: none;">Circuits Sons Etranges</a></div>


## Using Arduino

The next step was to build a circuit with arduino. It's a powerful electronic card with a microcontroller and several analogic and digital inputs/outputs.
It's great for prototyping new circuit and it accepts all kind of sensors and actuators.

Next exercise was to create a circuit with 2 motors which could be controlled by the arduino. Once again I used a schema already existing. The motors had to turn in one way or the other.
This is the code arduino I wrote to do this. I also added lines to increase every sec the speed of the motor:

```
#define A4 4
#define A5 5
#define B6 6
#define B7 7
int POWER = 125;
int increPW = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(A4, OUTPUT); // pin connectés aux moteurs A et B
  pinMode(A5, OUTPUT);
  pinMode(B6, OUTPUT);
  pinMode(B7, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(A4, POWER); // Le moteur A tourne dans le sens trigo quand A4=1 et A5=0
  digitalWrite(A5, LOW);
  digitalWrite(B6, POWER); // Le moteur B tourne dans le sens trigo inverse quand B6 =1 et B7=0
  digitalWrite(B7, LOW);

  POWER = POWER + increPW; // Le moteur tourne de plus en plus vite

  if (POWER >=255){   // Une fois la vitesse max atteinte, la vitesse revient à un niveau lent
    POWER = 35;
  }
  delay(1000);
}
```

One reference sheet very usefull : https://www.arduino.cc/reference/fr/


## Doing My own circuit

To "design" my own circuit, I searched on the web to find some inspiration :
This project caught my eye :

![image](docs/Images/projet_piano_arduino.PNG "Projet piano")

The project included to play a melody which I didn't want. But I added a note to make a pentatonic keyboard which is the base to improvise in any context.
Here is the first prototype I made :

![image](docs/Images/Proto_board_penta.JPG "1er Protoboard piano")

The code I used in Arduino :

```
#include "pitches.h"
#define ACTIVATED LOW


const int PIEZO = 11;   // Not a piezo in my circuit, but a simple speaker
const int LED = 13;

const int BUTTON_G = 8;
const int BUTTON_E = 6;
const int BUTTON_D = 4;
const int BUTTON_C = 3;
const int BUTTON_A = 2;


void setup()
{
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  pinMode(BUTTON_A, INPUT);
  digitalWrite(BUTTON_A,HIGH);
  pinMode(BUTTON_C, INPUT);
  digitalWrite(BUTTON_C,HIGH);
  pinMode(BUTTON_D, INPUT);
  digitalWrite(BUTTON_D,HIGH);
  pinMode(BUTTON_E, INPUT);
  digitalWrite(BUTTON_E,HIGH);
  pinMode (BUTTON_G, INPUT);
  digitalWrite(BUTTON_G, HIGH);

  digitalWrite(LED,LOW);
}

void loop()
{
   while(digitalRead(BUTTON_A) == ACTIVATED)
  {
    tone(PIEZO,NOTE_A4);
    digitalWrite(LED,HIGH);
  }
  while(digitalRead(BUTTON_C) == ACTIVATED)
  {
    tone(PIEZO,NOTE_C5);
    digitalWrite(LED,HIGH);
  }

  while(digitalRead(BUTTON_D) == ACTIVATED)
  {
    tone(PIEZO,NOTE_D5);
    digitalWrite(LED,HIGH);
  }

  while(digitalRead(BUTTON_E) == ACTIVATED)
  {
    tone(PIEZO,NOTE_E5);
    digitalWrite(LED,HIGH);
  }

    while(digitalRead(BUTTON_G) == ACTIVATED)
  {
    tone(PIEZO,NOTE_G5);
    digitalWrite(LED,HIGH);
  }

  noTone(PIEZO);
  digitalWrite(LED,LOW);

}
```

Since I also wanted to add a sensor to use the capacity of Arduino, I added a photoresistance which would change the tonality of the notes depending on the ligth. The darker it gets, the lower the notes are.

For the photoresistance, we need a voltage divider bridge to make it work :

<img src="docs/Images/2021-09-29_15_05_11-diviseur_photoresistance.png" alt="pont diviseur de tension" width="400">

I tested the resistance of the photoresistance when high luminosity with the multimeter :

<img src="docs/Images/Value_photoresistance.JPG" alt="valeur photoresistance" style="display: block; margin: 0 auto;" width="400">

I decided to use this value for the resistance in the voltage divider bridge. From what I understood a higher resistance would give a higher range but 1.8kOhm is acceptable.

Next step is to write the code, I found this one in Adafruit :

```
/* Photocell simple testing sketch.

Connect one end of the photocell to 5V, the other end to Analog 0.
Then connect one end of a 10K resistor from Analog 0 to ground

For more information see http://learn.adafruit.com/photocells */

int photocellPin = 0;     // the cell and 10K pulldown are connected to a0
int photocellReading;     // the analog reading from the analog resistor divider

void setup(void) {
  // We'll send debugging information via the Serial monitor
  Serial.begin(9600);   
}

void loop(void) {
  photocellReading = analogRead(photocellPin);  

  Serial.print("Analog reading = ");
  Serial.print(photocellReading);     // the raw analog reading

  // We'll have a few threshholds, qualitatively determined
  if (photocellReading < 10) {
    Serial.println(" - Dark");
  } else if (photocellReading < 200) {
    Serial.println(" - Dim");
  } else if (photocellReading < 500) {
    Serial.println(" - Light");
  } else if (photocellReading < 800) {
    Serial.println(" - Bright");
  } else {
    Serial.println(" - Very bright");
  }
  delay(1000);
}
```


I integrated this code into my pentatonic piano code :
```
#include "pitches.h"
#define ACTIVATED LOW


const int PIEZO = 11;
const int LED = 13;

const int BUTTON_G = 8;
const int BUTTON_E = 6;
const int BUTTON_D = 5;
const int BUTTON_C = 3;
const int BUTTON_A = 2;

#define photopin A0
int photoreading;


void setup()
{
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  digitalWrite(LED,LOW);
  pinMode(BUTTON_A, INPUT);
  digitalWrite(BUTTON_A,HIGH);
  pinMode(BUTTON_C, INPUT);
  digitalWrite(BUTTON_C,HIGH);
  pinMode(BUTTON_D, INPUT);
  digitalWrite(BUTTON_D,HIGH);
  pinMode(BUTTON_E, INPUT);
  digitalWrite(BUTTON_E,HIGH);
  pinMode (BUTTON_G, INPUT);
  digitalWrite(BUTTON_G, HIGH);
  pinMode(photopin, INPUT);


}

void loop()
{
  photoreading = analogRead(photopin);
  Serial.print("Analog reading =");
  Serial.print(photoreading);               // affichage dans COM de Analog reading = value de la photopin

    if(photoreading <300){					// High pitch ton when high luminosity
    Serial.print("très lumineux");
    while(digitalRead(BUTTON_A) == ACTIVATED)
    {
      tone(PIEZO,NOTE_A4);
      digitalWrite(LED,HIGH);
    }
    while(digitalRead(BUTTON_C) == ACTIVATED)
    {
      tone(PIEZO,NOTE_C5);
      digitalWrite(LED,HIGH);
    }

    while(digitalRead(BUTTON_D) == ACTIVATED)
    {
      tone(PIEZO,NOTE_D5);
      digitalWrite(LED,HIGH);
    }

    while(digitalRead(BUTTON_E) == ACTIVATED)
    {
      tone(PIEZO,NOTE_E5);
      digitalWrite(LED,HIGH);
    }

      while(digitalRead(BUTTON_G) == ACTIVATED)
    {
      tone(PIEZO,NOTE_G5);
      digitalWrite(LED,HIGH);
    }
  }
  else if(photoreading < 600){						// Medium pitch tonality
    Serial.print("un peu lumineux");
     while(digitalRead(BUTTON_A) == ACTIVATED)
    {
      tone(PIEZO,NOTE_A3);
      digitalWrite(LED,HIGH);
    }
    while(digitalRead(BUTTON_C) == ACTIVATED)
    {
      tone(PIEZO,NOTE_C4);
      digitalWrite(LED,HIGH);
    }

    while(digitalRead(BUTTON_D) == ACTIVATED)
    {
      tone(PIEZO,NOTE_D4);
      digitalWrite(LED,HIGH);
    }

    while(digitalRead(BUTTON_E) == ACTIVATED)
    {
      tone(PIEZO,NOTE_E4);
      digitalWrite(LED,HIGH);
    }

      while(digitalRead(BUTTON_G) == ACTIVATED)
    {
      tone(PIEZO,NOTE_G4);
      digitalWrite(LED,HIGH);
    }
  }
  else if(photoreading > 600){ 						// Low pitch when dark
    Serial.print(" noir");
      while(digitalRead(BUTTON_A) == ACTIVATED)
    {
      tone(PIEZO,NOTE_A2);
      digitalWrite(LED,HIGH);
    }
    while(digitalRead(BUTTON_C) == ACTIVATED)
    {
      tone(PIEZO,NOTE_C3);
      digitalWrite(LED,HIGH);
    }

    while(digitalRead(BUTTON_D) == ACTIVATED)
    {
      tone(PIEZO,NOTE_D3);
      digitalWrite(LED,HIGH);
    }

    while(digitalRead(BUTTON_E) == ACTIVATED)
    {
      tone(PIEZO,NOTE_E3);
      digitalWrite(LED,HIGH);
    }

      while(digitalRead(BUTTON_G) == ACTIVATED)
    {
      tone(PIEZO,NOTE_G3);
      digitalWrite(LED,HIGH);
    }
  }



    noTone(PIEZO);
  digitalWrite(LED,LOW);
}
```

I set up the conditional value of photoreading (ranging from 1 to 1024) after testing it. High luminosity was about 250 and dark was 700. Dark should be around 1000 but it probably depends on the quality of the photocell and the value of the resistance in the tension divider bridge.

The final protoboard :

<img src="docs/Images/Proto_board_penta_photo.JPG" alt="protoboard piano photoresistance" width="400">



## Eagle


Eagle is a software which allow for drawing electronic circuit to print them on card board.

Never close schematic when board is open and vice_versa.

<img src="docs/Images/EAGLE PNG" alt="" width="400">

Download first Adafruit and Sparkun libraries. Make sure that they are used -> right click -> use

<img src="docs/Images/EAGLE PNG" alt="" width="400">

Conception principle :

						- always put the connecting devices to the edge of the board.

						- Choose when possible to curve the angles (this seems to be an old design myth debunked here:https://www.nwengineeringllc.com/article/right-angle-pcb-traces-its-time-to-kill-the-myths.php)
						<img src="docs/Images/2021-09-29_15_59_02-NWES_Blog.png" alt="" width="200">

						- Ground plane : link to to the ground with a polygone surrounding the card -> use ratnest to fill the card.

						- DRC -> change the cutting thickness to 16 mil and also check clearances
						<img src="docs/Images/DRC PNG" alt="" width="200">

						- Build the card in modules to test connections independently. It's advised to use 0 Ohm resistances between modules.

						- In bigger circuit, add tests points, USB connections and text to name the connections.

						- Add capacity between + and -.

						- Export in 1200 dpi, black and white.

						- If needed, invert the colors with photoshop (or GIMP). The copper regions should be in white.

						- print the board on a sheet of paper first to check the size.

Also last thing before printing, in Layers -> Hide every layers but Top, pad and dimensions.

<img src="docs/Images/LAYERS PNG" alt="" width="200">

In options -> set -> miscellaneous -> deselect display pad/fill/drills

<img src="docs/Images/LAYERS PNG" alt="" width="200">

DRC -> check clear all

<img src="docs/Images/LAYERS PNG" alt="" width="200">

Also if needed to send the schematic to an external provider, convert it into gerber files. Gives info about each layers.


## Printing the copper card

I printed the schema on a sheet of paper to check the size. It helped me to save some time because the print was 2 times bigger than it should.


For the engraving preferences, Speed, Power, Frequence are set to 10, 100, 1 and fiber, raster with 1200 dpi are selected.

I had to do the process of cutting several times (more than 50) to finally cut through the glass fiber part. I noticed that it's pointless trying to cut the card when the copper is not engraved first... Also, since the laser has a very precise focus point and the card has a thickness of a few mm, it's good to move up the tray after a few passes.
The preferences of cutting I used are CO2, curve and 600 dpi with 15, 70 and 100 for the Speed, Power and Frequence.

| Schéma of the board | card engraving | Uneven engraving |
| - | - | - |
|<img src="docs/Images/Impression_schema_piano-1.png" alt="schema copper board" height="200" width="300">|<img src="docs/Images/decoupe_machine.JPG" alt="" height="200" width="300">|<img src="docs/Images/decoupe_carte_inegale.JPG" alt="" height="200" width="300">|

| Cutting of the card | Next iteration of cutting | Cutting the last bit with a cutter |
| - | - | - |
|<img src="docs/Images/decoupe_carte1.JPG" alt="" height="200" width="300">|<img src="docs/Images/decoupe_carte2.JPG" alt="" height="200" width="300">|<img src="docs/Images/decoupe_carte_finitions.JPG" alt="" height="200" width="300">|


## Soldering

I've already done some soldering before so I'm not entirely new to it. But SMD soldering is not an easy task since the components are just 2mm*1mm wide. So I checked a video before to get a few techniques on how to do it: https://www.youtube.com/watch?v=k3f_4-2dBhQ

I turned the heat to 350° and chosed the thinner tin wire I could find(5mm here). The technique is to add a tiny bit of tin on the copperboard and on one connector of the component and then place the component on the board while heating it.

| Soldering resistances | ... | ... |
| - | - | - |
|<img src="docs/Images/soudures_resistances1.JPG" alt="" width="300">|<img src="docs/Images/soudures_resistances2.JPG" alt="" width="300">|<img src="docs/Images/Soudures_resistances3.JPG" alt="" width="300">|

| Soldering photoresistance | Connectors | Interruptors |
|-|-|-|
|<img src="docs/Images2/soudures_photoresistance.JPG" alt="" width="300">|<img src="docs/Images2/soudure_connecteurs.JPG" alt="" width="300">|<img src="docs/Images2/soudure_interrupteurs.JPG" alt="" height="" width="300">|

| Test multimeter | test photoresistance | Test photoresistance passed |
| - | - | - |
|<img src="docs/Images2/test_multimeter.JPG" alt="" width="300">|<img src="docs/Images2/test_photoresistance.JPG" alt="" height="300" width="300">|<img src="docs/Images2/2021-10-01_13_51_15-Test_photoresistance.png" alt="" height="300" width="300">|


## Finitions

Last part goal is to make a little box for encasing the card and arduino board :
https://fr.makercase.com/#/ is a very user friendly website to make dxf files of boxes to use on laser cutting machine.

| Makercase site | Box assembly | Test of the whole piano |
| - | - | - |
|<img src="docs/Images2/2021-10-01_14_10_57-MakerCase.png" width="300">|<img src="docs/Images2/decoupe_boite.JPG" width="300">|<img src="docs/Images2/test_piano.JPG" width="300">|

Final look :

<img src="docs/Images2/Piano_final.JPG" width="">

A little recording of the piano (a range of 15 notes can be played with 5 interruptors thanks to the photoresistance):

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1139260831&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/user-887841969" title="Flo" target="_blank" style="color: #cccccc; text-decoration: none;">Flo</a> · <a href="https://soundcloud.com/user-887841969/piano-pentatonique" title="Piano Pentatonique" target="_blank" style="color: #cccccc; text-decoration: none;">Piano Pentatonique</a></div>
