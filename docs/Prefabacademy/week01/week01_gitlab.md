# Using Gitlab

Gitlab is an open source software which allow people to collaborate and share files (mostly in programming) with other people.

this software will allow me to document everything i learn during this year in DU Fabacademy. It will allow me and the evaluator to track down everything i'll learn and make.

![image](docs/Images/gitlab.PNG "Gitlab")

After installing gitbash, the first step was to figure out how to communicate with the command prompt.

![image](docs/Images/2021-09-27_17_45_32-github-git-cheat-sheet.png "Cheatsheet")
![image](docs/Images/2021-09-27_17_45_58-github-git-cheat-sheet.png "cheatsheet2")
Here it's some reference cheatsheet i found on the internet about git commands.

One usefull command I learned is
 ```
	Touch file_name
	mkdir dir_name
```
which creates a new file/directory.

The tutorial I found on the fabcloud to setup git:
http://pub.fabcloud.io/tutorials/week01_principles_practices_project_management/git_simple.html

After setting up the name, the email and generating the SSH Key on git, ![image](docs/Images/keygen.png "keygen")the goal is to upload the repo from the computer to the server git:

	git add .			//add all the repo
	git commit -m "what i just added" 	// allows to track the different commit added
	git push			//upload it on git

These are the 3 steps required to upload the repository on gitlab:
![image](docs/Images/test.png "git push")

The next step is to install python which will only be used to install mkdocs on it which is the software i'll use to make the internet site.
I had to make an internet research to figure out how to use python commands on the windows cmd :
![image](docs/Images/recherche_python.PNG "recherche internet cmd python")

Next step is to install mkdocs with pip in python:
![image](docs/Images/installation_mkdocs.PNG "cmd install mkdocs")

I made a test to see how my website looks like with the command mkdocs serve:
![image](docs/Images/test_mkdoc_serve.PNG "serve")

Markdowns improvements :

resizing images : ```<img src="docs/Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="200"/> ```

<img src="docs/Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="400"/>

With the add of the extra.css file :

![image alt ><](docs/Images4/2021-10-11_17_52_45-200Hz.png)

![image alt <](docs/Images4/2021-10-11_17_52_45-200Hz.png)

![image alt >](docs/Images4/2021-10-11_17_52_45-200Hz.png)

Table test

```
| text| colonn 2 |
| ------| -----|
| line2 | l2 c2
```

| text | colonn 2 |
| ------| -------|
| <img src="Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="300"/> | <img src="Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="200"/>	|
| <img src="docs/Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="200"/> | <img src="docs/Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="300"/> |

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

<div class="row">
	<div class="column">
	<img src="Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="300"/>
	</div>
	<div class="column">
	<img src="Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="300"/>
	</div>
	<div class="column">
	<img src="Images/2021-09-28_12_30_26-Overflow.png" alt="drawing" width="300"/>
	</div>

</div>

I'll continue to update this page when i'll learn new quality of life changes about markdowns and how to make an internet site...
