#Week 1 of DU Fabacademy

After having met Romain Di Vozzo (Director of Digiscope) and Jonah Ross-Marr (fabmanager), i learned how to use the laser cuting machine

##Laser cutting machine

The choice of materials is important when considering laser cutting (always check if the material is compatible)

- Security is primordial -> always activate the 2 security systems :

![Image](docs/Images/securite_laser.jpg "2 security systems to activate before cutting")

- Next step is positionning the calibrating tool :

![Image](docs/Images/Instruments_de_calibrage.jpg "Different calibrating tools for fiber and CO2 cutting")

- Lay the calibrating tool against the tray with the arrows on the display :

![Image](docs/Images/Interface_decoupe_laser.jpg "Interface")

- Define origin

- click on the controller to confirm origin

- Now we need to fix origin on CorelDraw

- right click to convert to curve or pixel (depending on the need)

- In width menu select hairline

- In print menu (ctrl + p) select the printer (here it's fusion) and click on preferences

- CO2 to cut or Fiber to wear out

- Modify the values of Power, Speed and Fqce depending on the materials in the reference sheet

![image](docs/Images/Valeurs_decoupe_laser.JPG "Speed Power Frequence")

(Alwys test materials when we never worked with them)

- Click Apply then Print

- Don't forget the security systems !

Always verify formats and lengths
Be careful to the hierarchy of the lane to avoid cutting a piece which could fall before another one -> break curve apart

 - Before beginning the cutting, verify the actual dimensions of the cut on the material by hitting the play button when the door is open -> the head of laser will still move but the laser won't since the door is open

The first test I made :
![image](docs/Images/decoupe_guitare.JPG "1st test")

The second one to test living hinge, but the drawing was too small and lines ended up being to close to each others.
![image](docs/Images/decoupe_guitare2.JPG "2nd test")

