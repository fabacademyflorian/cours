# CNC milling machine


In this page, I will document how to use the shopbot milling machine and my first experiments with it

<img src="docs/Images6/Shopbot_machine.JPG" width="550" align="middle">

Here is the slides Jonah Marrs made for the shopbot training :

<iframe src="//www.slideshare.net/slideshow/embed_code/key/gw7gx8k95KP9gJ" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/JonahRossMarrs/shop-bot-training" title="Shop bot training" target="_blank">Shop bot training</a> </strong> from <strong><a href="https://www.slideshare.net/JonahRossMarrs" target="_blank">Jonah Marrs</a></strong> </div>


## VCarve

This software allow to create a 2D shape or import a 2D (formats : dxf, dwg, ai, pdf) or 3D shape (stl, dxf, skp, 3ds, obj, v3m, lwo, 3dm)

<img src="docs/Images6/offset_xy.png" width="550" align="middle">

### Setup the material

First, you need to setup the origin. Also make sure the drawing is positioned to your liking and the configuration of the machine :

<img src="docs/Images6/SetOrigin.JPG" width="550" align="middle">

then define the thickness and dimensions of the material we are using.

<img src="docs/Images6/MaterialOrigin.JPG" height="400" align="">

### Toolpath

Then you need to define the way the tool will operate.

<img src="docs/Images6/Toolpath.JPG" width="" height="400" align="middle">

To fill the edit tool tabs, you need to know the dimensions of the bit and calculate inputs with the following site :

|Edit tool|Feed speeds calculation|
|-|-|
|<img src="docs/Images6/EditTool.JPG" height="400" width="">|[<center><img src="docs/Images6/Feed_speeds.png" height="400"><br/>https://pub.pages.cba.mit.edu/feed_speeds/</center>](https://pub.pages.cba.mit.edu/feed_speeds/)|

Choosing between climb and conventional depends on the material used and the quality of the surface wanted. Conventional is used for harder material like steel and climb is better to have a good surface quality.

You can choose to add tabs which allows your objects to not fall or move during the cut.

Further explanations of the different parts on the slides, or in the site above.

You can add different toolpaths for different operations or when you need to change bit or the position of the material.

When the toolpath is done, save it.

## Shopbot

This software uses the gcode imported from VCarve to send it to the machine :

<img src="docs/Images6/Shopbot.JPG" width="550" align="middle">

First, you need to setup the origin of the bit by moving it with the console and hit the zero button when the bit is in place.

<img src="docs/Images6/ZeroAxes.JPG" width="" width="550" align="middle">

You can chose to preview the cut before.

Hit the cut part button to import the file and start the cut. The software will prompt you to make sure all is well setup.

It's recommended to do an air path before to make sure that the global directions of the bit corresponds to what you want. For that you'll need to set the zero Z axes above your material for a distance corresponding to the depth of the cut.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Xa7-i7z7jvY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You'll need to fixate the material on the martyr with glue.

Last step before starting the cut : Warmup spindle (shortcut is C5). This operation lasts 15min.

## Security

Security is primordial : Always close the protective carter before starting the cut. Use the protective goggles and the soundproof headphones.

Clean the machine with a vacuum cleaner.

## First Experiment

For the first experiment, I followed the recommendations of Jonah to make something simple. I made a simple Square shape 2D profile and pocket path.

I then searched in the web for a good bit among those available in the fablab. I Choose to use the ZHS-500 which is good for soft materials and it is a square end mill (so it's good for surfacing) :

<img src="docs/Images6/fiche_fraise.png" width="550" align="middle">

This is the input I got from the Speed_feed calculator (I put 1/4 inches for the tool diameter for a conventional dimension even though it's 5 mm so ~0.2 inches)

<img src="docs/Images6/Feed_speeds1.PNG" width="" height="400" align="middle">

Here is the result :

<img src="docs/Images6/1st_test.JPG" width="550" align="middle">

## 2nd Experiment : 3D shape

For the 2nd experiment, I made a simple 3D shape. I drew a bawl shape with Fusion 360. My goal is to cut a shape with a curve and to test how I can flip the material upside down to machine the inside of the bawl and the outside without losing the origin.

<iframe name="3Dshape"  src="docs/3D/CNC_milling_3D.obj" height="550" frameborder="0"></iframe>

I will use the material from the previous test and I will need to surface it a little before doing the shape.

Here are the steps of the operations planned :
- Surface top (3 toolpaths)
- Inside of the bawl (1 roughing and 1 finishing toolpath)
- Make some mark to keep the origin before flipping the foam upside down (3 points toolpath)
- Surface former bottom (1 toolpath of 2.5 mm depth (the max allowed with the tool))
- Doing a ramp next to the bawl
- Outside of the bawl

And some captures of the first 2 steps on Vcarve and the results :

|Toolpaths|Surfacing + roughing preview|Finishing preview|
|-|-|-|
|<img src="docs/Images6/Bol3Dvcarve.png">|<img src="docs/Images6/Bol3Droughing.png">|<img src="docs/Images6/Bol3DFinishing.png">|
|<img src="docs/Images6/Surfacing.JPG">|<img src="docs/Images6/Roughing.JPG">|<img src="docs/Images6/Finishing.JPG">|

For the finishing tool I switched for a ball end mill of 3 mm diameter :

|Specs on the Misumi website|Tool edit with speed feeds|
|-|-|
|<img src="docs/Images6/Ball_end_mill.png" height="400">|<img src="docs/Images6/ToolFinishing.JPG" height="400">|

### Keeping the origin when flipping the material

This is a crucial step and probably the most difficult one for this test. Since the machine is 3 axes and has no rotation possible around the X and Y axis we have to flip the material upside down to make a 3D object. But we have to make sure that the material is correctly centred.

To flip the material, I have chosen to make wooden dowels. I will use 2 of them as a frame of reference in the XY plane. I will take the coordinates of the edge of the dowels due to the bulkiness of the tool carrier. Also, the wooden dowels will act as a holder for the foam since I've done an operation of roughing of 1cm depth.

Placement of the wooden dowels :

<img src="docs/Images6/marks1.JPG" width="550">

Then I realised I forgot to take into account a metal part attached to the tool carrier. So, here is the 2nd iteration :

|Metal part attached to the tool carrier|2nd iteration of reference dowels|
|-|-|
|<img src="docs/Images6/metal_part.JPG" height="400">|<img src="docs/Images6/marks2.JPG" height="400">|

Here are the coordinates of the marks, the origin is in the bottom left of the material. I also took the coordinates of the center of the bawl :

|Based coordinates|Inverted coordinates|
|-|-|
|<img src="docs/Images6/Coord1.png" height="">|<img src="docs/Images6/Coord_inverted.png" height="">|

Now that the coordinates are retrieved, I can flip the material. After calibrating the new origin, I made 2 marks on the martyr, corresponding to the 2 inverted coordinates, with the help of the drill mill. Then, I tried to align the tips of the wooden dowels to the marks. It's not really precise but it seems well enough for my first test :

<img src="docs/Images6/marks_flip2.JPG" width="550" height="">

---------

First operation on this face is a roughing of 2cm

then I'll need to do a ramp in order to not being obstructed when the bit is going lower. Here is a schematic to explain what I'll do. I initially went for a wider ramp but changed my mind to only cut what is necessary. The tool carrier has a diameter of 3.5 cm so each step have to be at least 1.75cm long. The end mill is about 2.5 cm long so each step have to be less than 2.5 cm high :

<img src="docs/Images6/ramp_schematic.JPG" width="550" height="">

Here are the toolpaths on Vcarve and the result :

|Toolpaths on Vcarve|Milling of the ramp|
|-|-|
|<img src="docs/Images6/2Dview_VCarve.JPG" height="400">|<img src="docs/Images6/ramp_result.JPG" height="400" height="">

Now for the last step, I imported the same 3D shape as before but turned it upside down in VCarve.

<img src="docs/Images6/Bawl_bottom.JPG" width="550" height="">

Here is the preview of the toolpaths :

<img src="docs/Images6/AllToolpaths.JPG" width="550" height="">

And the final result :

|Final result : looks good...|... Not so much|
|-|-|
|<img src="docs/Images6/machining_result.JPG" height="">|<img src="docs/Images6/Bawl.JPG" height="">

There is a visible offset which is due to the operation of flipping the material. The way I did to retrieve the coordinates and to do some marks on the martyr is not really precise and I'll need to improve this for the next time.

Last operation in video :

<iframe width="560" height="315" src="https://www.youtube.com/embed/AC9DTDGzcr0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

we can see in the last part, that the bawl moved because I forgot to make tabs for the object. For this reason I couldn't do the finishing path on the outside of the ball.

### Ideas to improve the "flipping operation"

There was 2 main problem leading to the offset between top 3D part and bottom 3D part :

1. The fact that I retrieved the coordinates of the wooden dowels and the center of the bawl after doing the first 2 steps (surfacing and milling the inside of the bawl)

2. The fact that I tried to align the tips of the dowels to the marks made with a pen on the martyr

Here are some suggestions I will test for the next time :

1. Plan the flipping of the material <b>before</b> starting the milling. Realise the milling of the bawl according to the placement of the 2 reference points.

2. Realise a hole in the dowels and the martyr at the emplacement of the reference point and fixate one point with a screw as a rotation axis while aligning the other point.

3. Last but not least, insert <b>tabs</b>
