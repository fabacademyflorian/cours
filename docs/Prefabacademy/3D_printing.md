## Printing with Ultimaker 3

I'm going to make some test for clipping two parts together.
I made the drawing part on Fusion 360, here are the dimensions of the 3 designs I'll try :

|1st one 2mm extrusion|2nd one 3mm extrusion|3d one 4mm extrusion|
|-|-|-|
|<img src="docs/Images7/2021-11-08_17_37_22.png">|<img src="docs/Images7/2021-11-09_V2.png">|<img src="docs/Images7/2021-11-09_V3.png">

<div class="sketchfab-embed-wrapper"> <iframe title="Test clips" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share src="https://sketchfab.com/models/b9d105b1059b46bebc29895f3188758e/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/test-clips-b9d105b1059b46bebc29895f3188758e?utm_medium=embed&utm_campaign=share-popup&utm_content=b9d105b1059b46bebc29895f3188758e" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Test clips </a> by <a href="https://sketchfab.com/Flowber?utm_medium=embed&utm_campaign=share-popup&utm_content=b9d105b1059b46bebc29895f3188758e" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Flowber </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=b9d105b1059b46bebc29895f3188758e" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

I will make a serie of test with these 3 designs and the following printing conditions :
- 0.8 mm head
- 0.4 mm head
- 0.25 mm head
- resin printer


### 1st test and setup of the printing machine

For the filament, I used NGEN ColorFabb blue. I searched on the web for the characteristics and printing settings of this filament. I found them on makershop website, ColorFabb website and Filimprimant3D.fr:

|characteristics on makershop|Settings in Ultimaker Cura|More details on filimprimant3D.fr|
|-|-|-|
|<img src="docs/Images7/2021-11-08_12_42_55.png" width="">|<img src="docs/Images7/2021-11-08_12_54_11.png" width="">|<img src="docs/Images7/2021-11-09_15_43_32.png" width="">|

There was a step of Z calibration of the tray. I did it a with a metal ring of approximately 1.6 mm thickness instead of a 1mm because it was what I had close at hand. For the refine calibration, I used the calibration card.

I had some issue with one test because the filament intake was freezing a little bit. Also another test failed because the glass plate wasn't correctly locked in place as we can see in the next picture (thankfully, it was short prints of 10min) :

<img src="docs/Images7/fail.JPG" width="550">

### Results with a 0.8 mm head :

|1st design|2nd design|3d design|
|-|-|-|
|<img src="docs/Images7/v1_0.8.JPG" width="">|<img src="docs/Images7/v2_0.8.JPG" width="">|<img src="docs/Images7/v3_0.8.JPG" width="">

None of those design worked with a 0.8 mm head, the first one because the clip part was too small for a 0.8mm head, the other 2 because the clip part was too sturdy and couldn't fit into the hole. there are other tests to be done with a 0.8 mm head.

### Results with a 0.4 mm head :

|1st design|2nd design|3d design|
|-|-|-|
|<img src="docs/Images7/v1_0.4.JPG" width="">|<img src="docs/Images7/v2_0.4.JPG" width="">|<img src="docs/Images7/v3_0.4.JPG" width="">|

For the same reason as before, the 2nd and 3d designs are not good ones.

I had some issue with warping of the parts :

<img src="docs/Images7/warping.JPG" width="550">

The settings were not correct here, I changed the heating of the bed to 85 °C and the printing temperature to 230 °C (for some reason I didn't save those correctly) and I slowed down the printing speed to be sure that the filament is getting cold between each layer.

The 1st design has been made with the good settings and the results are good.

### Results with a 0.25 mm head :

For these tests, I changed the 2nd and 3d designs to be less sturdy :

|1st design with assembly|2nd design|3d design|
|-|-|-|
|<img src="docs/Images7/v1_0.25.JPG" width="">|<img src="docs/Images7/v2_v3_0.8.JPG" width="">|<img src="docs/Images7/v2_v3_0.8.JPG" width="">|
|<img src="docs/Images7/v1_0.25_assemblage.JPG" width="">|||

The 1st design works with a 0.25 mm head, but the settings were not the good ones. I did this printing before I found out that the temperature were not correct as mentioned before. It also doesn't seem long lasting.

### Results with the resin printer :
